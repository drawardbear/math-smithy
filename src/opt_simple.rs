use num_traits::real::Real;
use num_traits::{Signed, signum};

struct OptSettings<R: Real> {
    step_size: R,
    convergence_threshold: R,
    epsilon: R,
}

#[derive(Debug)]
struct OptResult<R: Real> {
    min_val: R,
    arg_min: R
}

fn gradient_descent<R: Real + Signed>(f: &dyn Fn(R) -> R, x_0: R, settings: OptSettings<R>) -> OptResult<R> {
    let mut x = x_0;
    let mut improvement = Real::max_value();

    while improvement > settings.convergence_threshold {
        let d = sample_derivative(f, x, settings.epsilon);
        let x_last = x;
        x = x - settings.step_size * signum(d);
        improvement = f(x_last) - f(x);
    }

    OptResult {
        min_val: f(x),
        arg_min: x,
    }
}

fn sample_derivative<R: Real>(f: &dyn Fn(R) -> R, x: R, h: R) -> R {
    (f(x+h) - f(x)) / h
}

#[cfg(test)]
mod tests {
    use num_traits::abs;
    use crate::opt_simple::{gradient_descent, sample_derivative, OptSettings};

    #[test]
    fn test_sample_derivative() {
        let result = sample_derivative(&(|x| (x-3.0) * (x-3.0) + 3.0), 0.0, 0.005);
        assert!(abs(result - -6.0) < 0.01);

        let result = sample_derivative(&(|x| (x-3.0) * (x-3.0) + 3.0), 3.0, 0.005);
        assert!(abs(result - 0.0) < 0.01);

        let result = sample_derivative(&(|x| (x-3.0) * (x-3.0) + 3.0), 1.0, 0.005);
        assert!(abs(result - -4.0) < 0.01);
    }

    #[test]
    fn test_gradient_descent(){
        let result = gradient_descent(&(|x| (x-2.0) * (x-2.0) + 3.0), 0.0, OptSettings {
            step_size: 0.001,
            convergence_threshold: 0.000001,
            epsilon: 0.005,
        });
        assert!(abs(result.arg_min - 2.0) < 0.005);
        assert!(abs(result.min_val - 3.0) < 0.00001);
    }
}