mod opt_simple;

use std::fmt::{Debug, Formatter};
use std::ops;
use std::cmp::{min, max};

pub trait BinaryOperator<D> {
    fn eval(a: D, b: D) -> D;
}

pub trait UnitaryOperator<D> {
    fn eval(a: D) -> D;
}

struct N {}

struct Z {}

struct Q {
    p: u32,
    q: u32,
}

impl Q {
    fn simplify(&mut self) -> &mut Self {
        let gcd = gcd(self.p, self.q);
        self.p = self.p / gcd;
        self.q = self.q / gcd;
        self
    }
}

impl Debug for Q {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} / {}", self.p, self.q)
    }
}

impl ops::Add<&Q> for &Q {
    type Output = Q;

    fn add(self, rhs: &Q) -> Self::Output {
        let mut z = Q {
            p: self.p * rhs.q + self.q * rhs.p,
            q: self.q * rhs.q
        };
        z.simplify();
        z
    }
}

impl ops::Mul<&Q> for &Q {
    type Output = Q;

    fn mul(self, rhs: &Q) -> Self::Output {
        let mut z = Q {
            p: self.p * rhs.p,
            q: self.q * rhs.q
        };
        z.simplify();
        z
    }
}

fn gcd(a: u32, b: u32) -> u32 {
    let mut r_next = min(a, b);
    let mut r = max(a, b);

    while r_next != 0 {
        let m = r / r_next;
        let r_new = r_next;
        r_next = r - m * r_next;
        r = r_new;
    }

    r
}

fn main() {
    let x = Q {p : 13, q :  3};
    let y = Q {p: 2, q: 3};
    println!("{:?}", &x + &y);
    println!("{:?}", &x * &y);
    println!("{:?}", gcd(21, 6));
}
